# assignments

Here are assignments and final project of this course.

- HW1: Logistic Regression, KNN, Genetic Algorithm
- HW2: Decision Tree, Basic Neural Network
- HW3: K-Means Clustering, Statistical Classification and Naive Bayes
- HW4: Q-Learning (Reinforcement Learning)
- Final Project: Q-Learning (Reinforcement Learning) [advanced]