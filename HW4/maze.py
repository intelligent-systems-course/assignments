import numpy as np
import pandas as pd
mazeMap = pd.read_csv('ENV1.map', sep = ',', header = None)
states = []
for i in range(mazeMap.shape[0]):
    for j in range(mazeMap.shape[1]):
        states.append((i,j))
Qmap = pd.DataFrame(0, index = ['n', 'e', 's', 'w'], columns = states)
gamma = 0.9
alpha = 0.2
beta = 1.
R = 100
C = 2
L = 20
punish = -20
P = 0.3
start = [(x, mazeMap.columns[y]) for x, y in zip(*np.where(mazeMap.values == 1))][0]
end = [(x, mazeMap.columns[y]) for x, y in zip(*np.where(mazeMap.values == 3))][0]

def findpossibleActions(state):
    possible = []
    if (state[0] > 0):
        possible.append('n')
    if (state[0] < mazeMap.shape[0] - 1):
        possible.append('s')
    if (state[1] > 0):
        possible.append('w')
    if (state[1] < mazeMap.shape[1] - 1):
        possible.append('e')
    return possible

def findMaxQforSPrime(s):
    s_actions = findpossibleActions(s)
    maxQ_s_a = -np.inf
    for a in s_actions:
        if Qmap[s][a] >= maxQ_s_a:
            maxQ_s_a = Qmap[s][a]
    return maxQ_s_a

def findState(state, a):
    if (a == 'n'):
        s_prime = (state[0]-1,state[1])
    if (a == 's'):
        s_prime = (state[0]+1,state[1])
    if (a == 'w'):
        s_prime = (state[0],state[1]-1)
    if (a == 'e'):
        s_prime = (state[0],state[1]+1)
    return s_prime

def softmax(a, s, all_a):
    p1 = np.exp(beta * Qmap[s][a])
    p2 = 0.
    for u in all_a:
        p2 += np.exp(beta * Qmap[s][u])
    return p1/p2

def chooseAction(state, actions):
    actions_prob = []
    for a in actions:
        actions_prob.append(softmax(a, state, actions))
    pr = np.array(actions_prob)
    indx = np.argmax(pr)
    chosenAction = actions[indx]
    num = float(np.random.uniform(0,1,1))
    if num > P:
        return chosenAction
    else:
        del actions[indx]
        pr = np.delete(pr,indx)
        if len(actions) == 1:
            return actions[0]
        pr /= sum(pr)
        n = int(np.random.choice(len(actions), 1, p = pr))
        chosenAction = actions[n]
        return chosenAction

scores = []
for k in range(3000):
    #x1 = int(np.random.choice(mazeMap.shape[0], 1))
    #x2 = int(np.random.choice(mazeMap.shape[1], 1))
    current = start #(x1,x2)
    RewardMap = mazeMap.copy()
    RewardMap[RewardMap == 0] = -C
    RewardMap[RewardMap == 1] = 0
    RewardMap[RewardMap == 2] = -L
    RewardMap[RewardMap == 3] = R
    scores.append(0)
    while current != end:
        possibleActions = findpossibleActions(current)
        selectedAction = chooseAction(current, possibleActions)
        reward = RewardMap[current[1]][current[0]]
        scores[-1] += reward
        s_prime = findState(current, selectedAction)
        Qmap[current][selectedAction] += alpha*(reward + gamma * findMaxQforSPrime(s_prime) - Qmap[current][selectedAction])
        RewardMap[current[1]][current[0]] = punish
        current = s_prime
    print(k)
from matplotlib import pyplot as plt
plt.plot(scores)
plt.show()

# test the algorithm
current = start
RewardMap = mazeMap.copy()
RewardMap[RewardMap == 0] = -C
RewardMap[RewardMap == 1] = 0
RewardMap[RewardMap == 2] = -L
RewardMap[RewardMap == 3] = R
testScore = 0
road = [current]
while current != end:
    possibleActions = findpossibleActions(current)
    selectedAction = chooseAction(current, possibleActions)
    reward = RewardMap[current[1]][current[0]]
    testScore += reward
    s_prime = findState(current, selectedAction)
    current = s_prime
    road.append(current)
for i in range(len(road)):
    road[i] = str(road[i])
print(' -> '.join(road))