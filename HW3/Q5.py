import numpy as np
import pandas as pd
train = pd.read_csv('Q5/Mushroom_Train.csv', sep = ',')
test = pd.read_csv('Q5/Mushroom_Test.csv', sep = ',')
trainNum = train.shape[0]
testNum = test.shape[0]
featuresNum = train.shape[1]

classes = list(set(train['class']))
ProbC = []
for c in classes:
	ProbC.append(sum(train['class'] == c)/trainNum)

features = train.columns
features = [features[i] for i in range(len(features)) if features[i] != 'class']
ProbWandC = []
for f in features:
	featureValue = list(set(train[f]))
	probFeature = pd.DataFrame(index = classes, columns = featureValue)
	for c in classes:
		for w in featureValue:
			a = train[f] == w
			b = train['class'] == c
			probFeature[w][c] = (sum(a&b)+1)/(sum(b)+len(featureValue))
	ProbWandC.append(probFeature)

def calcProb(featuresValues, c):
	pr = 1.
	for i in range(len(ProbWandC)):
		pr *= ProbWandC[i][featuresValues[i]][c]
	return pr

def maxProb(p):
	maxP = 0
	label = ''
	for i in range(len(p)):
		if p[i][1] >= maxP:
			maxP = p[i][1]
			label = p[i][0]
	return label

labelGuess = testNum * ['']
for t in range(testNum):
	vals = list(test.loc[t])
	probs = []
	for c in classes:
		probs.append((c,calcProb(vals[1:], c)));
	labelGuess[t] = maxProb(probs);

def calcAccAndConf():
	conf = pd.DataFrame(0 , index = classes, columns = classes)
	acc = 0
	for t in range(testNum):
		vals = list(test.loc[t])
		if vals[0] == labelGuess[t]:
			acc += 1
		conf[labelGuess[t]][vals[0]] += 1
	return [(acc/testNum)*100, conf]

[accuracy, confusinMat] = calcAccAndConf();
print("Accuracy:", accuracy, "%")
print("====================\n",confusinMat)