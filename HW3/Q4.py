import numpy as np
import math
trainData = 0.01 * np.genfromtxt('Fashion_MNIST/Train_Data.csv', delimiter = ',')
trainLabels = np.genfromtxt('Fashion_MNIST/Train_Labels.csv', delimiter = ',')
testData = 0.01 * np.genfromtxt('Fashion_MNIST/Test_Data.csv', delimiter = ',')
testLabels = np.genfromtxt('Fashion_MNIST/Test_Labels.csv', delimiter = ',')

trainDataNum = trainData.shape[0]
testDataNum = testData.shape[0]
featuresNum = trainData.shape[1]
labelsNum = 10

def multivariateNormal(x, mu, sigma):
	size = len(x)
	if size == len(mu) and (size, size) == sigma.shape:
		det = np.linalg.det(sigma)
		if det == 0.:
			raise NameError("The covariance matrix can't be singular")

		norm_const = 1.0/ (math.pow((2*np.pi),float(size)/2) * math.pow(det,1.0/2))
		x_mu = np.matrix(x - mu)
		inv = np.linalg.inv(sigma)
		result = math.pow(math.e, -0.5 * (x_mu * inv * x_mu.T))
		return norm_const * result
	else:
		raise NameError("The dimensions of the input don't match")

train = [np.zeros((1,featuresNum))] * labelsNum
for i in range(trainDataNum):
	for l in range(labelsNum):
		if trainLabels[i] == l:
			train[l] = np.append(train[l],[trainData[i]],axis=0)
for l in range(labelsNum):
	train[l] = np.delete(train[l],0,axis=0)
print("splitting completed.")

mu = [np.zeros((1,featuresNum))] * labelsNum
for l in range(labelsNum):
	mu[l] = np.mean(train[l],axis = 0)

sigma = [np.zeros((1,featuresNum,featuresNum))] * labelsNum
for l in range(labelsNum):
	sigma[l] = np.cov(train[l].T)

Idn = np.eye(featuresNum)
alpha = 0.001
for l in range(labelsNum):
	sig = np.trace(sigma[l])/featuresNum
	sigma[l] = (1-alpha) * sigma[l] + alpha * sig * Idn
print("mean and covariance computed.")

### Part a & c ###

conf_A = np.array([[0] * labelsNum] * labelsNum)
labelGuess_a = np.array(testDataNum * [0])
for i in range(testDataNum):
	p = np.zeros(labelsNum)
	for l in range(labelsNum):
		p[l] = multivariateNormal(testData[i], mu[l], sigma[l])
	labelGuess_a[i] = np.argmax(p)	
	pred_l = int(labelGuess_a[i])
	true_l = int(testLabels[i])
	conf_A[pred_l][true_l] += 1

accuracy = (np.sum(np.equal(labelGuess_a, testLabels))/testDataNum)*100
print("Part A: Accuracy:", accuracy, "%")

### Part b & c ###

def classProb(label):
	return train[label].shape[0]/trainDataNum

conf_B = np.array([[0] * labelsNum] * labelsNum)
labelGuess_b = np.array(testDataNum * [0])
for i in range(testDataNum):
	p = np.zeros(labelsNum)
	for l in range(labelsNum):
		p[l] = multivariateNormal(testData[i], mu[l], sigma[l]) * classProb(l)
	labelGuess_b[i] = np.argmax(p)
	pred_l = int(labelGuess_b[i])
	true_l = int(testLabels[i])
	conf_B[pred_l][true_l] += 1

accuracy = (np.sum(np.equal(labelGuess_b, testLabels))/testDataNum)*100
print("Part B: Accuracy:", accuracy, "%\n")

print("Confusion Matrix of Part A\n")
print("_________________________ Correct Classes from 0 to 9 _________________________")
for i in range(labelsNum):
	print("Class",i,":",conf_A[i])
	print("___________")
print("\n")
print("Confusion Matrix of Part B\n")
print("_________________________ Correct Classes from 0 to 9 _________________________")
for i in range(labelsNum):
	print("Class",i,":",conf_B[i])
	print("___________")