import numpy as np
import pandas as pd
import random
trainData = np.genfromtxt('Q1/Train_Data.csv', delimiter = ',')
trainLabels = np.genfromtxt('Q1/Train_Labels.csv', delimiter = ',')
testData = np.genfromtxt('Q1/Test_Data.csv', delimiter = ',')
testLabels = np.genfromtxt('Q1/Test_Labels.csv', delimiter = ',')

trainDataNum = trainData.shape[0]
testDataNum = testData.shape[0]
featuresNum = trainData.shape[1]
k = 3

### Part A ###

def initM():
	_m = np.zeros((k,featuresNum))
	for i in range(k):
		for j in range(featuresNum):
			_m[i][j] = random.uniform(min((trainData.T)[j]),max((trainData.T)[j]))
	return _m

m = initM()

def findNearestCentroid(data, Mean):
	nearCent = 0
	distance = np.inf
	for j in range(k):
		x = np.linalg.norm(data - Mean[j])
		if x <= distance:
			distance = x
			nearCent = j
	return nearCent

def updateCentroids(Cent):
	for i in range(trainDataNum):
		for j in range(k):
			if C[i] == j:
				clusteredTrainData[j] = np.append(clusteredTrainData[j],[trainData[i]],axis=0)
				clusteredTrainDataLabels[j] = np.append(clusteredTrainDataLabels[j],[trainLabels[i]],axis=0)
	cMean = np.zeros((k,featuresNum))
	cLabel = np.zeros((k,))
	for j in range(k):
		clusteredTrainData[j] = np.delete(clusteredTrainData[j],0,axis=0)
		clusteredTrainDataLabels[j] = np.delete(clusteredTrainDataLabels[j],0,axis=0)
		cMean[j] = np.mean(clusteredTrainData[j], axis=0)
		cLabel[j] = np.argmax(np.bincount(clusteredTrainDataLabels[j].astype('int32')))
	return [cMean, cLabel]

C = [0] * trainDataNum
flag = True
while flag == True:
	flag = False
	clusteredTrainData = [np.zeros((1,featuresNum))] * k
	clusteredTrainDataLabels = [np.zeros(1)] * k
	for i in range(trainDataNum):
		C[i] = findNearestCentroid(trainData[i], m)
	[newM, mLabel] = updateCentroids(C)
	for i in range(k):
		for j in range(featuresNum):
			if m[i][j] != newM[i][j]:
				flag = True
	m = newM
	if flag == False and len(set(mLabel)) != k:
		m = initM()
		flag = True

confusinMat = pd.DataFrame(0 , index = range(1,k+1), columns = range(1,k+1))
testGuess = [0.] * testDataNum
testAcc = 0
for i in range(testDataNum):
	testGuess[i] = findNearestCentroid(testData[i], m)
	testGuess[i] = mLabel[testGuess[i]]
	if testGuess[i] == testLabels[i]:
		testAcc += 1
	confusinMat[testGuess[i]][testLabels[i]] += 1
testAcc = (testAcc/testDataNum)*100
print("Test Data Accuracy of myScript:", testAcc, "%")
print("====================\n",confusinMat)

### Part B ###

from sklearn.cluster import KMeans
kmeans = KMeans(n_clusters = k)
kmeans.fit(trainData)
l = kmeans.labels_
clusteredTrainData = [np.zeros((1,featuresNum))] * k
clusteredTrainDataLabels = [np.zeros(1)] * k
for i in range(trainDataNum):
	for j in range(k):
		if l[i] == j:
			clusteredTrainDataLabels[j] = np.append(clusteredTrainDataLabels[j],[trainLabels[i]],axis=0)
cl = np.zeros((k,))
for j in range(k):
	clusteredTrainDataLabels[j] = np.delete(clusteredTrainDataLabels[j],0,axis=0)
	cl[j] = np.argmax(np.bincount(clusteredTrainDataLabels[j].astype('int32')))
pred = kmeans.predict(testData)
labelsGuess = pred
for i in range(len(pred)):
	labelsGuess[i] = cl[pred[i]]

KmeansConfusinMat = pd.DataFrame(0 , index = range(1,k+1), columns = range(1,k+1))
testAcc2 = 0
for i in range(testDataNum):
	if labelsGuess[i] == testLabels[i]:
		testAcc2 += 1
	KmeansConfusinMat[labelsGuess[i]][testLabels[i]] += 1
testAcc2 = (testAcc2/testDataNum)*100
print("Test Data Accuracy of Scikit Learn:", testAcc2, "%")
print("====================\n",KmeansConfusinMat)