from skimage import io
import numpy as np
import random
image = io.imread('Q2/landscape.jpg') 
rows, cols = image.shape[0],image.shape[1]
image = image.reshape(rows*cols,3)
trainData = image
trainDataNum = trainData.shape[0]
featuresNum = trainData.shape[1]
k = 50

def initM():
	_m = np.zeros((k,featuresNum))
	for i in range(k):
		for j in range(featuresNum):
			_m[i][j] = random.uniform(min((trainData.T)[j]),max((trainData.T)[j]))
	return _m

m = initM()

def findNearestCentroid(data, Mean):
	distances = np.zeros((k,trainDataNum))
	for j in range(k):
		distances[j] = np.linalg.norm(data - Mean[j], axis = 1)
	nearCent = np.argmin(distances, axis = 0)
	return nearCent

C = np.array([0] * trainDataNum)
flag = True
while flag == True:
	flag = False
	newM = np.zeros((k,featuresNum))
	C = findNearestCentroid(trainData, m)
	for j in range(k):
		if trainData[C == j].shape[0] == 0:
			newM[j] = 0
		else:
			newM[j] = np.mean(trainData[C == j], axis = 0)
	for i in range(k):
		for j in range(featuresNum):
			if m[i][j] != newM[i][j]:
				flag = True
	m = newM

## with scikit-learn
# from sklearn.cluster import KMeans
# kmeans = KMeans(n_clusters = k)
# kmeans.fit(trainData)
# C = kmeans.labels_
# clusteredTrainData = [np.zeros((1,featuresNum))] * k
# m = kmeans.cluster_centers_

labels = C
clusters = m
labels = labels.reshape(rows,cols);  

image = np.zeros((rows,cols,3),dtype=np.uint8 )
for i in range(rows):
    for j in range(cols):
            image[i,j,:] = clusters[labels[i,j],:]
io.imsave('compressed_image'+'_K_'+str(k)+'.jpg',image);
io.imshow(image)
io.show()