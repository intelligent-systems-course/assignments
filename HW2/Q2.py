import numpy as np
import pandas as pd
trainData = np.array(pd.read_csv('Fashion-MNIST/trainData.csv', sep = ',', header = None))
trainLabels = np.genfromtxt('Fashion-MNIST/trainLabels.csv', delimiter = ',')
testData = np.genfromtxt('Fashion-MNIST/testData.csv', delimiter = ',')
testLabels = np.genfromtxt('Fashion-MNIST/testLabels.csv', delimiter = ',')

trainDataNum = trainData.shape[0]
testDataNum = testData.shape[0]
featuresNum = trainData.shape[1]
hiddenLayersNum = 1
neuronsNum = 200
classesNum = 10
layers = hiddenLayersNum + 1

distance = lambda x,y : np.linalg.norm(x-y, 2)
loss = lambda x,y : distance(x,y) ** 2
Relu = lambda x : np.maximum(x, np.array(x.shape[0] * [0]))

weight = layers * [np.array()]
bias = layers * [np.array()]

weight[0] = np.random.randn(neuronsNum, featuresNum)
bias[0] = np.random.randn(neuronsNum, 1)
weight[-1] = np.random.randn(classesNum, neuronsNum)
bias[-1] = np.random.randn(classesNum, 1)

if hiddenLayersNum > 1:
	for i in range(1,hiddenLayersNum):
		weight[i] = np.random.randn(neuronsNum,neuronsNum)

for epoch in range(1000):
	for k in range(trainDataNum):
		h[0] = Relu(np.dot(weight[i], trainData[k]) + bias[i])
		if hiddenLayersNum > 1:
			for i in range(1,hiddenLayersNum):
				h[i] = Relu(np.dot(weight[i], h[i-1]) + bias[i])
		y_hat = Relu(np.dot(weight[-1], h[-1]) + bias[-1])
		y = np.array(classesNum * [0])
		y[trainLabels[k]] = 1 
		L = loss(y_hat, y)
		grad_w_L = 2 * distance(y_hat, y) * (1 * ())