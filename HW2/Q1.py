import numpy as np
data = np.genfromtxt('arrhythmia.data', delimiter = ',', filling_values = np.inf)
data = np.array([x for x in data if np.inf not in x])
trainData = data[:40]
testData = data[40:]
NumOfLabels = 16

def isLinear(attr):
	if len(set(list(trainData[:,attr]))) > 2:
		return 1
	else:
		return 0

attributes = [(i,isLinear(i)) for i in range(1,data.shape[1])]

def entropy(data):
	temp = np.copy(data)
	unique, counts = np.unique(temp, return_counts=True)
	if len(counts) < 2:
		return 0
	p0 = counts[0]/(counts[0]+counts[1])
	p1 = 1 - p0
	if p1 == 0 or p0 == 0:
		return 0
	ent = -p0 * np.log2(p0) - p1 * np.log2(p1)
	return ent

def informationGain(data, attr):
	temp = np.copy(data)
	rootEntropy = entropy(temp[:,-1])
	if attr[1] == 0:
		values = np.array(list(set(list(temp[:,attr[0]]))))
		group1 = []
		group2 = []
		for i in range(len(temp[:,attr[0]])):
			if temp[i][attr[0]] == values[0]:
				group1.append(list(temp[i]))
			elif temp[i][attr[0]] == values[1]:
				group2.append(list(temp[i]))
		group1 = np.array(group1)
		group2 = np.array(group2)
		if group1.shape[0] == 0 or group2.shape[0] == 0:
			return [0, list(group1), list(group2), 0.5]
		child1Ent = len(group1[:,attr[0]]) * entropy(group1[:,-1])/len(temp[:,attr[0]])
		child2Ent = len(group2[:,attr[0]]) * entropy(group2[:,-1])/len(temp[:,attr[0]])
		gain = rootEntropy - child1Ent - child2Ent
		return [gain, list(group1), list(group2), 0.5]
	else:
		threshold = np.median(temp[:,attr[0]])
		values = np.array(list(set(list(temp[:,attr[0]]))))
		group1 = []
		group2 = []
		for i in range(len(temp[:,attr[0]])):
			if temp[i][attr[0]] <= threshold:
				group1.append(list(temp[i]))
			else:
				group2.append(list(temp[i]))
		group1 = np.array(group1)
		group2 = np.array(group2)
		if group1.shape[0] == 0 or group2.shape[0] == 0:
			return [0, list(group1), list(group2), threshold]
		child1Ent = (len(group1[:,attr[0]]) * entropy(group1[:,-1]))/len(temp[:,attr[0]])
		child2Ent = (len(group2[:,attr[0]]) * entropy(group2[:,-1]))/len(temp[:,attr[0]])
		gain = rootEntropy - child1Ent - child2Ent
		return [gain, list(group1), list(group2), threshold]
		
def findBestAttribute(data, attrs):
	maxGain = 0
	bestAttr = (None,None)
	group1 = []
	group2 = []
	for a in attrs:
		retVal = informationGain(data, a)
		g = retVal[0]
		tempGroup1 = retVal[1]
		tempGroup2 = retVal[2]
		tempTreshold = retVal[3]
		if g > maxGain:
			maxGain = g
			bestAttr = a
			group1 = tempGroup1
			group2 = tempGroup2
			treshold = tempTreshold
	return [bestAttr, group1, group2, treshold]

class Node:
	def __init__(self, examples=None, attributes=None, targetAttribute=(None,None), threshold=None, rightChild=None, leftChild=None, label=None):
		self.examples = examples
		self.attributes = attributes
		self.targetAttribute = targetAttribute
		self.threshold = threshold
		self.rightChild = rightChild
		self.leftChild = leftChild
		self.label = label

	def __str__(self, level=0):
		if self.targetAttribute[0] != None:
			ret = '\t'*level+repr(self.targetAttribute[0])+'\n'
		else:
			ret = '\t'*level+'class: '+repr(self.label)+'\n'
		if self.rightChild != None:
			ret += (self.rightChild).__str__(level+1)
		if self.leftChild != None:
			ret += (self.leftChild).__str__(level+1)
		return ret

	def __repr__(self):
		return '<tree node representation>'

def ID3(examples, attributes):
	root = Node(examples=examples, attributes=attributes)
	if not 0 in root.examples[:,-1]:
		root.label = False
		return root
	elif not 1 in root.examples[:,-1]:
		root.label = True
		return root
	elif len(attributes) == 0:
		root.label = not bool(np.argmax(np.bincount(root.examples[:,-1])))
		return root
	else:
		retVal = findBestAttribute(root.examples, root.attributes)
		bestAttr = retVal[0]
		group1 = np.copy(np.array(retVal[1]))
		group2 = np.copy(np.array(retVal[2]))
		threshold = retVal[3]
		childAttr = root.attributes.copy()
		childAttr.remove(bestAttr)
		root.targetAttribute = bestAttr
		root.threshold = threshold
		root.leftChild = ID3(group1, childAttr)
		root.rightChild = ID3(group2, childAttr)
	return root

trees = NumOfLabels*[Node()]
for i in range(1,NumOfLabels+1):
	examples = np.copy(trainData)
	examples[:,-1] = [1*(i!=x) for x in examples[:,-1]]
	trees[i-1] = ID3(examples, attributes)
	strClass = '======== Class '+str(i)+' ========'
	print(strClass)
	print(trees[i-1])

def testlabel(root, testDatum, num):
	predLabel = 0
	if root.label == True:
		predLabel = num
	elif root.label == None:
		if root.targetAttribute[1] == 1:
			if testDatum[root.targetAttribute[0]] <= root.threshold:
				predLabel = testlabel(root.leftChild, testDatum, num)
			else:
				predLabel = testlabel(root.rightChild, testDatum, num)
		else:
			if testDatum[root.targetAttribute[0]] == 0:
				predLabel = testlabel(root.leftChild, testDatum, num)
			else:
				predLabel = testlabel(root.rightChild, testDatum, num)
	return predLabel

def testTrees(test):
	accuracy = 0
	for x in test:
		predictedLabels = NumOfLabels*[0]
		for i in range(NumOfLabels):
			predictedLabels[i] = testlabel(trees[i], x, i+1)
		pred = list(set(predictedLabels))
		if len(pred) == 2:
			if pred[1] == x[-1]:
				accuracy += 1
	return accuracy/test.shape[0]

acc = testTrees(testData)
print("=================================")
strAcc = "Overall Accuracy : %" + str(acc*100)
print(strAcc)

def testOneVsAllTrees(test):
	accuracy = NumOfLabels*[0]
	for i in range(NumOfLabels):
		for x in test:
			predictedLabels = testlabel(trees[i], x, i+1)
			if x[-1] == i+1:
				if predictedLabels == x[-1]:
					accuracy[i] += 1
			else:
				if predictedLabels == 0:
					accuracy[i] += 1
	return accuracy

acc = testOneVsAllTrees(testData)
for i in range(NumOfLabels):
	print("=================================")
	strAcc = "One Vs. All Accuracy for tree " + str(i+1) + ": %" + str((acc[i]/testData.shape[0])*100)
	print(strAcc)