password = list(input())
passwordDictionary = dict()
for i in range(0,53):
	if i < 26:
		passwordDictionary[chr(i + ord('A'))] = i
	elif i < 52:
		passwordDictionary[chr(i - 26 + ord('a'))] = i
	else:
		passwordDictionary[' '] = i

def replaceByKey(password):
	for i in range(0,len(password)):
		password[i] = passwordDictionary[password[i]]
replaceByKey(password)

import numpy as np

goal = np.array(password)
passSize = len(goal)
goal = np.append(goal, passSize)

k = 1000
passGuess = np.array(np.full((k,len(goal)), 0))
for i in range(0,k):
	passGuess[i][:-1] = np.random.randint(low = 0, high = 53, size = passSize)
	passGuess[i][-1] = np.sum(np.equal(passGuess[i][:-1], goal[:-1]))
passGuess = passGuess[passGuess[:, -1].argsort()]

numOfIter = 0
while True:
	if passGuess[-1][-1] == goal[-1]:
		break
	for j in range(0,10):
		passGuess[j] = passGuess[j+k-10]
	for i in range(10,k,2):
		a = np.append(passGuess[i][:int(passSize/3)], passGuess[i+1][int(passSize/3):-1])
		b = np.append(passGuess[i+1][:int(2*passSize/3)], passGuess[i][int(2*passSize/3):-1])
		mutationA = np.random.randint(low = 0, high = 53, size = 1)
		a[int(np.random.randint(low = 0, high = passSize, size = 1))] = mutationA
		mutationB = np.random.randint(low = 0, high = 53, size = 1)
		b[int(np.random.randint(low = 0, high = passSize, size = 1))] = mutationB
		costA = np.sum(np.equal(a, goal[:-1]))
		costB = np.sum(np.equal(b, goal[:-1]))
		passGuess[i] = np.append(a,costA)
		passGuess[i+1] = np.append(b,costB)
	passGuess = passGuess[passGuess[:, -1].argsort()]
	numOfIter += 1

def replaceByValue(guess):
	password = [''] * len(guess)
	for i in range(0,len(guess)):
		password[i] = [k for k,v in passwordDictionary.items() if v == int(guess[i])]
		password[i] = password[i][0]
	return ''.join(password)
trueGuess = replaceByValue(list(passGuess[-1][:-1]))
print('Number of iterations:', numOfIter)
print('Your password is:', trueGuess)