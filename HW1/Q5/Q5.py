import numpy as np
trainData = np.genfromtxt('Fashion_MNIST/Train_Data.csv', delimiter = ',')
trainLabels = np.genfromtxt('Fashion_MNIST/Train_Labels.csv', delimiter = ',')
trainLabels = trainLabels.astype('int32') 
testData = np.genfromtxt('Fashion_MNIST/Test_Data.csv', delimiter = ',')

trainDataNum = trainData.shape[0]
testDataNum = testData.shape[0]

distance = np.zeros((testDataNum,trainDataNum))
for i in range(0,testDataNum):
	for j in range(0,trainDataNum):
		distance[i][j] = np.linalg.norm(testData[i]-trainData[j], 2)

testLabels = np.genfromtxt('Fashion_MNIST/Test_Labels.csv', delimiter = ',')
testLabels = testLabels.astype('int32')
labelGuess = np.zeros((testDataNum,))
labelGuess = labelGuess.astype('int32')
resultFor2 = np.array(np.full((testDataNum,2), 0))
resultFor5 = np.array(np.full((testDataNum,5), 0))
resultFor10 = np.array(np.full((testDataNum,10), 0))
resultFor15 = np.array(np.full((testDataNum,15), 0))

for i in range(0,testDataNum):
	temp = distance[i].argsort()
	resultFor2[i] = temp[:2]
	resultFor5[i] = temp[:5]
	resultFor10[i] = temp[:10]
	resultFor15[i] = temp[:15]

### K=2 ###
for i in range(0,testDataNum):
	counts = np.bincount(trainLabels[resultFor2[i]])
	labelGuess[i] = np.argmax(counts)
accuracyFor2 = np.sum(np.equal(labelGuess, testLabels))/testDataNum
print('Accuracy for K=2:',accuracyFor2)
labelGuess = labelGuess.astype('float')
np.savetxt('Fashion_MNIST/testFor2.csv', [labelGuess], delimiter = '\n')
labelGuess = labelGuess.astype('int32')

### K=5 ###
for i in range(0,testDataNum):
	counts = np.bincount(trainLabels[resultFor5[i]])
	labelGuess[i] = np.argmax(counts)
accuracyFor5 = np.sum(np.equal(labelGuess, testLabels))/testDataNum
print('Accuracy for K=5:',accuracyFor5)
labelGuess = labelGuess.astype('float')
np.savetxt('Fashion_MNIST/testFor5.csv', [labelGuess], delimiter = '\n')
labelGuess = labelGuess.astype('int32')

### K=10 ###
for i in range(0,testDataNum):
	counts = np.bincount(trainLabels[resultFor10[i]])
	labelGuess[i] = np.argmax(counts)
accuracyFor10 = np.sum(np.equal(labelGuess, testLabels))/testDataNum
print('Accuracy for K=10:',accuracyFor10)
labelGuess = labelGuess.astype('float')
np.savetxt('Fashion_MNIST/testFor10.csv', [labelGuess], delimiter = '\n')
labelGuess = labelGuess.astype('int32')

### K=15 ###
for i in range(0,testDataNum):
	counts = np.bincount(trainLabels[resultFor15[i]])
	labelGuess[i] = np.argmax(counts)	
accuracyFor15 = np.sum(np.equal(labelGuess, testLabels))/testDataNum
print('Accuracy for K=15:',accuracyFor15)
labelGuess = labelGuess.astype('float')
np.savetxt('Fashion_MNIST/testFor15.csv', [labelGuess], delimiter = '\n')
labelGuess = labelGuess.astype('int32')